﻿
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Application.Interfaces.Persistence
{
    public interface ISaleRepository
    {
        void Add(Sale sale);
        void Update(Sale sale);
        Sale GetSaleById(Guid Id);
    }
}
