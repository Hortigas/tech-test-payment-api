﻿namespace Pottencial.Payment.Domain.Entities
{
    public class Item
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string ItemId { get; set; } = null!;
        public string Description { get; set; } = null!;
        public float Value { get; set; }
    }
}
