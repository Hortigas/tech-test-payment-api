﻿namespace Pottencial.Payment.Domain.Entities
{
    public class Sale
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime SaleDate { get; set; }
        public Seller seller { get; set; } = null!;
        public IList<Item> items { get; set; } = null!;
    }
}
